<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataGameDominoesTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_game_dominoes_tracker', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hosted_game_id')->index('hosted_game_id');
            $table->foreignId('user_id')->nullable()->index('user_id');//nullable as nullable is tiles belonging to no one or the house eg users can pick from this pile of tiles.
            $table->foreignId('domino_id')->index('domino_id');
            $table->timestamps();
        });

        Schema::table('data_game_dominoes_tracker', function (Blueprint $table) {
            $table->foreign('hosted_game_id')->references('id')->on('data_hosted_games')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('data_users')->onDelete('RESTRICT');
            $table->foreign('domino_id')->references('id')->on('definition_dominoes')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_game_dominoes_tracker');
    }
}
