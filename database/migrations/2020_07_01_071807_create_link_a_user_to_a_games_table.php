<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkAUserToAGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_users_to_a_game', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hosted_game_id')->index('hosted_game_id');
            $table->foreignId('user_id')->index('user_id');
            $table->timestamps();
        });

        Schema::table('link_users_to_a_game', function (Blueprint $table) {
            $table->foreign('hosted_game_id')->references('id')->on('data_hosted_games')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('data_users')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_users_to_a_game');
    }
}
