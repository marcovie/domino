<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataHostedGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_hosted_games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index('user_id');
            $table->foreignId('winner_user_id')->nullable()->index('winner_user_id');
            $table->string('description');
            $table->smallInteger('number_of_players');
            $table->boolean('joinable');//1 joinable - 0 - not joinable
            $table->boolean('game_open_or_closed');//1 open - 0 closed - 1 means game still active - 0 means closed due to game finished or inactive
            $table->timestamps();
        });

        Schema::table('data_hosted_games', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('data_users')->onDelete('RESTRICT');
            $table->foreign('winner_user_id')->references('id')->on('data_users')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_hosted_games');
    }
}
