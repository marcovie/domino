<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefinitionDominoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('definition_dominoes', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->smallInteger('tile_side_one');
            $table->smallInteger('tile_side_two');
            $table->boolean('tile_double');//True - tile is a double tile - false tile is not a double tile
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('definition_dominoes');
    }
}
