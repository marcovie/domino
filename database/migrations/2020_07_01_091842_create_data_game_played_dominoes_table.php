<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataGamePlayedDominoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_game_played_dominoes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hosted_game_id')->index('hosted_game_id');
            $table->foreignId('user_id')->index('user_id');
            $table->foreignId('domino_id')->index('domino_id');
            $table->foreignId('game_played_dominoes_parent_id')->index('game_played_dominoes_parent_id');
            $table->boolean('can_be_played_on');//this to see if this tile still available to be played on 1- Yes -> 0 -No
            $table->string('available_slots');//example double 5 is 5|5|5|5, remove each one when gets used, so we know what left to be played on
            $table->timestamps();
        });

        Schema::table('data_game_played_dominoes', function (Blueprint $table) {
            $table->foreign('hosted_game_id')->references('id')->on('data_hosted_games')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('data_users')->onDelete('RESTRICT');
            $table->foreign('domino_id')->references('id')->on('definition_dominoes')->onDelete('RESTRICT');
            $table->foreign('game_played_dominoes_parent_id')->references('id')->on('data_game_played_dominoes')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_game_played_dominoes');
    }
}
