<?php

use Illuminate\Database\Seeder;

class DefinitionDominoes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('definition_dominoes')->truncate();

        //--------------------------------------------------------------------------------------------Tile side one is all zeros--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
              'description' => 'Double zero tile',//Blank
              'tile_side_one' => 0,
              'tile_side_two' => 0,
              'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
              'created_at' => now()->format('Y-m-d H:i:s'),
              'updated_at' => now()->format('Y-m-d H:i:s'),
          ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and one tile',
            'tile_side_one' => 0,
            'tile_side_two' => 1,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and two tile',
            'tile_side_one' => 0,
            'tile_side_two' => 2,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and three tile',
            'tile_side_one' => 0,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and four tile',
            'tile_side_one' => 0,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and five tile',
            'tile_side_one' => 0,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Zero and six tile',
            'tile_side_one' => 0,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all ones--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double one tile',
            'tile_side_one' => 1,
            'tile_side_two' => 1,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'One and two tile',
            'tile_side_one' => 1,
            'tile_side_two' => 2,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'One and three tile',
            'tile_side_one' => 1,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'One and four tile',
            'tile_side_one' => 1,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'One and five tile',
            'tile_side_one' => 1,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'One and six tile',
            'tile_side_one' => 1,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all twos--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double two tile',
            'tile_side_one' => 2,
            'tile_side_two' => 2,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Two and three tile',
            'tile_side_one' => 2,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Two and four tile',
            'tile_side_one' => 2,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Two and five tile',
            'tile_side_one' => 2,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Two and six tile',
            'tile_side_one' => 2,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all threes--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double three tile',
            'tile_side_one' => 3,
            'tile_side_two' => 3,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Three and four tile',
            'tile_side_one' => 3,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Three and five tile',
            'tile_side_one' => 3,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Three and six tile',
            'tile_side_one' => 3,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all fours--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double fours tile',
            'tile_side_one' => 4,
            'tile_side_two' => 4,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Four and five tile',
            'tile_side_one' => 4,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Four and six tile',
            'tile_side_one' => 4,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all fives--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double fives tile',
            'tile_side_one' => 5,
            'tile_side_two' => 5,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => 'Five and six tile',
            'tile_side_one' => 5,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all sixes--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => 'Double sixes tile',
            'tile_side_one' => 6,
            'tile_side_two' => 6,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
    }
}
