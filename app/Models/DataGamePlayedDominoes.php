<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataGamePlayedDominoes extends Model
{
    const TABLE_NAME = 'data_game_played_dominoes';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
