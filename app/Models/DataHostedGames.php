<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\HasOne;

class DataHostedGames extends Model
{
    const TABLE_NAME    = 'data_hosted_games';

    const JOINABLE      = 1;
    const OPEN_GAME     = 1;

    protected $guarded = [];

    protected $table = self::TABLE_NAME;

    public function scopeFetchHostedGames($query, $user_id)
    {
        return $query->where('joinable', self::JOINABLE)->where('game_open_or_closed', self::OPEN_GAME)->where('user_id', '!=', $user_id)->with('UsersName');
    }

    public function scopeFetchGamesIHaveOpen($query, $user_id)
    {
        return $query->where('joinable', self::JOINABLE)->where('game_open_or_closed', self::OPEN_GAME)->where('user_id', $user_id)->with('UsersName');
    }

    public function UsersName(): HasOne
    {
        return $this->hasOne(DataUsers::class, 'id', 'user_id')->select(['id', 'name']);
    }
}
