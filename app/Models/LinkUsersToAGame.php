<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkUsersToAGame extends Model
{
    const TABLE_NAME = 'link_users_to_a_game';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
