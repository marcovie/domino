<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataGameDominoesTracker extends Model
{
    const TABLE_NAME = 'data_game_dominoes_tracker';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
