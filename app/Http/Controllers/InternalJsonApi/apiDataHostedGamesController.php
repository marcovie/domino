<?php

namespace App\Http\Controllers\InternalJsonApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Exception;
use Validator;

use App\Models\DataHostedGames;
use App\Models\LinkUsersToAGame;
use App\Models\DefinitionDominoes;
use App\Models\DataGameDominoesTracker;

use App\Helpers\Utility;

class apiDataHostedGamesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index() //Fetch Games
    {
        $user = Auth::user();

        try {
            // DB::beginTransaction();
            $DataHostedGames_Result = DataHostedGames::FetchHostedGames($user->id)->get();

            if(!is_null($DataHostedGames_Result) && count($DataHostedGames_Result) > 0)
            {
                return response()->json([
                    'successful' => 1,
                    'message'    => 'Successfully loaded.',
                    'data' => $DataHostedGames_Result
                ]);
            }

             return response()->json(array('successful' => 3, 'message' => 'No data found.'));
            // DB::commit();
        } catch (\Exception $e) {
            // DB::rollBack();
            return response()->json(['successful' => 0, 'message' => 'An error occurred, please try again']);
        }
    }

    public function store(Request $request) //Host a  Game
    {
        Utility::stripXSS($request);

        $request->validate([
          'numberOfPlayers' => 'required|integer|min:2|max:4'
        ]);

        try {
            $user = Auth::user();

            DB::beginTransaction();
            $DataHostedGames_Result = DataHostedGames::FetchGamesIHaveOpen($user->id)->get();

            if(!is_null($DataHostedGames_Result) && count($DataHostedGames_Result) > 0)
            {
                return response()->json([
                    'successful' => 0,
                    'message'    => 'Sorry a game is already in progress.'
                ]);
            }
            else
            {
                $DataHostedGamesObj                         = new DataHostedGames;
                $DataHostedGamesObj->user_id                = $user->id;
                $DataHostedGamesObj->description            = 'Game hosted';//This field might be removed
                $DataHostedGamesObj->number_of_players      = $request->numberOfPlayers;
                $DataHostedGamesObj->joinable               = DataHostedGames::JOINABLE;
                $DataHostedGamesObj->game_open_or_closed    = DataHostedGames::OPEN_GAME;

                $DataHostedGamesCreated                     = $DataHostedGamesObj->save();

                $LinkUsersToAGameObj                        = new LinkUsersToAGame;
                $LinkUsersToAGameObj->hosted_game_id        = $DataHostedGamesObj->id;
                $LinkUsersToAGameObj->user_id               = $user->id;

                $DefinitionDominoes_Result = DefinitionDominoes::all()->toArray();

                $DominoesCreationForGame_Result = true;
                foreach($DefinitionDominoes_Result as $key => $internalArrayData)
                {
                    $DataGameDominoesTrackerObj                     = new DataGameDominoesTracker;
                    $DataGameDominoesTrackerObj->hosted_game_id     = $DataHostedGamesObj->id;
                    $DataGameDominoesTrackerObj->domino_id          = $internalArrayData['id'];

                    if(!$DataGameDominoesTrackerObj->save())
                        $DominoesCreationForGame_Result = false;
                }

                if($DataHostedGamesCreated && $LinkUsersToAGameObj->save() && $DominoesCreationForGame_Result)
                {
                    DB::commit();
                    return response()->json([
                        'successful' => 3,
                        'message'    => 'Successfully loaded.',
                        'data' => $DataHostedGamesObj
                    ]);
                }
            }

             DB::rollBack();
             return response()->json(array('successful' => 0, 'message' => 'An error occurred, please try again'));
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['successful' => 0, 'message' => 'An error occurred, please try again'.$e]);
        }
    }
}
