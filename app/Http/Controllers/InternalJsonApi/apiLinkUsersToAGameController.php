<?php

namespace App\Http\Controllers\InternalJsonApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Exception;
use Validator;

use App\Models\DataGameDominoesTracker;

class apiLinkUsersToAGameController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($game_id) //Fetch Games
    {
        $user = Auth::user();
dd($user);
        try {
            // DB::beginTransaction();
            $DataGameDominoesTracker_Result = DataGameDominoesTracker::where('hosted_game_id', $game_id)->get();

            if(!is_null($DataGameDominoesTracker_Result) && count($DataGameDominoesTracker_Result) > 0)
            {
                return response()->json([
                    'successful' => 6,
                    'message'    => 'Successfully loaded.',
                    'data' => $DataGameDominoesTracker_Result,
                    'functionName' => 'TileBoneYard'
                ]);
            }

             return response()->json(array('successful' => 3, 'message' => 'No data found.'));
            // DB::commit();
        } catch (\Exception $e) {
            // DB::rollBack();
            return response()->json(['successful' => 0, 'message' => 'An error occurred, please try again']);
        }
    }
}
