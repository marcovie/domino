<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api', 'throttle:250,1')->namespace('InternalJsonApi')->group(function () {
    Route::resource('fetch-hosted-games-list', 'apiDataHostedGamesController');//This might change eg instead of invokable
    Route::resource('fetch-game-dominoes-tracker', 'apiDataGameDominoesTrackerController');//This might change eg instead of invokable
    Route::resource('link-user-to-a-game', 'apiLinkUsersToAGameController');//This might change eg instead of invokable

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
