@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <b>Project :</b>
        </div>
        <div class="col-md-6">
            Domino game
        </div>
        <div class="col-md-6">
            <b>Git Repo:</b>
        </div>
        <div class="col-md-6">
            <a href="https://bitbucket.org/marcovie/domino/src/master/" target="_blank">https://bitbucket.org/marcovie/domino/src/master/</a>
        </div>
        <div class="col-md-6">
            <b>Database Design:</b>
        </div>
        <div class="col-md-6">
            <a href="https://dbdiagram.io/d/5efc11b90425da461f0416fd" target="_blank">https://dbdiagram.io/d/5efc11b90425da461f0416fd</a>
        </div>
        <div class='col-md-12 mt-5'>
            <h1><u><b>Instructions</b></u></h1>
        </div>
        <div class='col-md-12'>
            <p>
                - It's played by a minimum of 2 players and a max of 4
            </p>
            <p>
                - 28 domino pieces are laid upside down randomly
            </p>
            <p>
                - Each piece is divided in half and has two sets of dots in each half. Each set can have 0 to 6 dots.
            </p>
            <p>
                - Each player picks 7 pieces.
            </p>
            <p>
                - The player with the bigger double starts
            </p>
            <p>
                - After the first piece, they go in turns placing pieces that match the same number of dots. If they don't have one, they pick another piece from the table.
            </p>
            <p>
                - The game ends when a player runs out of pieces or there are no more pieces that match and no more pieces left on the table
            </p>
            <p>
                - If both players still have pieces in the end, the winner is the one with the least total dots. These are calculated by adding the dots from all the pieces in the player's hand.
            </p>
        </div>
    </div>
</div>
@endsection
